namespace WfManager.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WfManager.Entites;

    internal sealed class Configuration : DbMigrationsConfiguration<WfManager.DataAccess.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WfManager.DataAccess.DataContext";
        }

        protected override void Seed(WfManager.DataAccess.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            if(!context.WorkflowStates.Any())
                   context.WorkflowStates.AddRange(new WorkflowState[] {
                    new WorkflowState { Name = "Initiated", Description = "The workflow has been created" },
                    new WorkflowState { Name = "On going" },
                    new WorkflowState { Name = "Finished", Description = "The workflow is finished" },
                     new WorkflowState { Name = "Stoped", Description = "The workflow is stoped" },
                      new WorkflowState { Name = "Canceled", Description = "The workflow is canceled" }
                    });
            if (!context.WorkflowTypes.Any())
                context.WorkflowTypes.AddRange(new WorkflowType[]
                {
                    new WorkflowType{Name="Acquisition"},
                    new WorkflowType{Name="Conservation"},
                    new WorkflowType{Name="Safety duplication"},
                    new WorkflowType{Name="Distribution"},
                    new WorkflowType{Name="Rejuvenation"},

                });
            if (!context.IdentifierTypes.Any())
                context.IdentifierTypes.AddRange(new IdentifierType[]
                {
                    new IdentifierType{Name="Sample GUID"},
                    new IdentifierType{Name="Accession Number"},
                    new IdentifierType{Name="Temporary Identifier"}
                });
        }
    }
}
