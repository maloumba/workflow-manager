﻿using Microsoft.EntityFrameworkCore;
using rbcEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
  public  class RbcDbContext:DbContext
    {
        public RbcDbContext(DbContextOptions<RbcDbContext> options)
       :base(options) {
         
        }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowAction> WorkflowActions { get; set; }
        public DbSet<WorkflowDetail> WorkflowDetails { get; set; }
        public DbSet<WorkflowState> WorkflowState { get; set; }
        public DbSet<WorkflowType> WorkflowTypes{ get; set; }
    }
}
