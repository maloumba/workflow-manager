﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rbcEntities
{
   public class Workflow:rbcActionBase
    {
        public int Id { get; set; }
        public WorkflowState WorkflowState { get; set; }
        public int workflowStateId { get; set; }
        public int WorkflowTypeId { get; set; }
        public WorkflowType WorkflowType { get; set; }
        public int Year { get; set; }
        public ICollection<WorkflowAction> WorkflowDetails { get; set; }

    }
}
