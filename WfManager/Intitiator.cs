﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfManager.Core;

namespace WfManager
{
    public class Initiator
    {
       public IUnitOfWork _businessClass { get; private set; }
        public Initiator(IUnitOfWork unitOfWork)
        {
            _businessClass = unitOfWork;
        }
    }
}
