﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicturesManager
{
      class Repository
        {
            private string _currentfilePath;
            public Repository()
            {

            }
            public Repository(string filePath)
            {

                if (File.Exists(filePath))
                {
                    Console.WriteLine("This file has been found!");
                    _currentfilePath = filePath;
                }
                else
                {
                    throw new Exception("The file specified does not exist!");
                }
            }


            internal Dictionary<string, string> GetfileContent(string filepath, char csvsymbole)
            {
                Dictionary<string, string> datafile = new Dictionary<string, string>();//Key/Value pair file data for accenumb and accename

                StreamReader reader = new StreamReader(File.OpenRead(filepath));
                string[] lignes = reader.ReadToEnd().Split(new string[] { Environment.NewLine },StringSplitOptions.None);
                for (int i = 0; i < lignes.Length - 1; i++)
                {

                    string[] keyvalue = lignes[i].Split(new char[] { csvsymbole });
                    datafile[keyvalue[0]] = keyvalue[1];
                }
                return datafile;
            }

            internal string GetAcceNumb(string accename, char csvsymbole)
            {
                var data = GetfileContent(_currentfilePath, csvsymbole);
                string accenumb = data.FirstOrDefault(a => a.Value.Equals(accename)||a.Key.Equals(accename)).Key;
                //Return the accenumb which is the key in the dictionary
                return accenumb;
            }

        }
}
