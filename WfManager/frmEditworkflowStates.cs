﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WfManager.Entites;

namespace WfManager
{
    public partial class frmEditworkflowStates : Form
    {
        Initiator initiator = DependencyInjector.Retrieve<Initiator>();
       public WorkflowState CurentWorkflowstate { get; set; }
        public frmEditworkflowStates()
        {
            InitializeComponent();           
        }

        private void frmEditworkflowStates_Load(object sender, EventArgs e)
        {
            ComboxStates.DataSource = initiator._businessClass.WorkflowStateRepository.GetAll();
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            CurentWorkflowstate = ComboxStates.SelectedItem as WorkflowState;
            this.Close();
        }
    }
}
