﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfManager.Entites;

namespace WfManager.DataAccess
{
  public  class DataContext:DbContext
    {
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowAction> WorkflowActions { get; set; }
        public DbSet<WorkflowDetail> WorkflowDetails { get; set; }
        public DbSet<WorkflowState> WorkflowStates { get; set; }
        public DbSet<WorkflowType> WorkflowTypes { get; set; }
        public DbSet<ActionbyWorkflowType> ActionbyWorkflowTypes { get; set; }
        public DbSet<IdentifierType> IdentifierTypes { get; set; }
        public DbSet<Pas_Accession> Pas_Accessions { get; set; }
        public DbSet<gg_inventory> Inventory { get; set; }
    }
}
