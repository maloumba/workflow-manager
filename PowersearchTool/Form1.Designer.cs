﻿namespace PowersearchTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.linkDataConfiguration = new System.Windows.Forms.LinkLabel();
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.checkBoxAccname = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.linkDataConfiguration);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 38);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search data:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(86, 50);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(287, 20);
            this.txtSearch.TabIndex = 2;
            // 
            // linkDataConfiguration
            // 
            this.linkDataConfiguration.AutoSize = true;
            this.linkDataConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkDataConfiguration.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkDataConfiguration.Location = new System.Drawing.Point(614, 20);
            this.linkDataConfiguration.Name = "linkDataConfiguration";
            this.linkDataConfiguration.Size = new System.Drawing.Size(173, 16);
            this.linkDataConfiguration.TabIndex = 0;
            this.linkDataConfiguration.TabStop = true;
            this.linkDataConfiguration.Text = "Data base configuration";
            // 
            // groupBoxData
            // 
            this.groupBoxData.Location = new System.Drawing.Point(86, 110);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Size = new System.Drawing.Size(654, 314);
            this.groupBoxData.TabIndex = 4;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = "DataGrid";
            // 
            // checkBoxAccname
            // 
            this.checkBoxAccname.AutoSize = true;
            this.checkBoxAccname.Location = new System.Drawing.Point(380, 52);
            this.checkBoxAccname.Name = "checkBoxAccname";
            this.checkBoxAccname.Size = new System.Drawing.Size(99, 17);
            this.checkBoxAccname.TabIndex = 5;
            this.checkBoxAccname.Text = "Accename only";
            this.checkBoxAccname.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkBoxAccname);
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkDataConfiguration;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.CheckBox checkBoxAccname;
    }
}

