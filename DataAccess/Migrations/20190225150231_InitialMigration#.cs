﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IdentifierType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentifierType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowActions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowActions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowState",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowState", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Workflows",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    CreateBy = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    UpdateBy = table.Column<string>(nullable: true),
                    InitiatedBy = table.Column<string>(nullable: true),
                    TerminatedBy = table.Column<string>(nullable: true),
                    workflowStateId = table.Column<int>(nullable: false),
                    WorkflowTypeId = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workflows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workflows_WorkflowTypes_WorkflowTypeId",
                        column: x => x.WorkflowTypeId,
                        principalTable: "WorkflowTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Workflows_WorkflowState_workflowStateId",
                        column: x => x.workflowStateId,
                        principalTable: "WorkflowState",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WorkflowId = table.Column<int>(nullable: false),
                    MaId = table.Column<string>(nullable: true),
                    IdentifierTypeId = table.Column<int>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    QtyWdrMTS = table.Column<float>(nullable: false),
                    CurrentQty = table.Column<float>(nullable: false),
                    HeatingstartDate = table.Column<DateTime>(nullable: true),
                    HeatingEndDate = table.Column<DateTime>(nullable: true),
                    Pre_germinationDate = table.Column<DateTime>(nullable: true),
                    NurseryDate = table.Column<DateTime>(nullable: true),
                    SentToFieldDate = table.Column<DateTime>(nullable: true),
                    SowingDate = table.Column<DateTime>(nullable: true),
                    SowedBy = table.Column<string>(nullable: true),
                    HarvestDate = table.Column<DateTime>(nullable: true),
                    HarvestBy = table.Column<string>(nullable: true),
                    winnowingDate = table.Column<DateTime>(nullable: true),
                    PreDryDate = table.Column<DateTime>(nullable: true),
                    startDryingDate = table.Column<DateTime>(nullable: true),
                    EndDryingDate = table.Column<DateTime>(nullable: true),
                    WADried = table.Column<bool>(nullable: false),
                    PackedForCleaningDate = table.Column<DateTime>(nullable: true),
                    CleaningStartDate = table.Column<DateTime>(nullable: true),
                    CleaningEndDate = table.Column<DateTime>(nullable: true),
                    CleanedBy = table.Column<string>(nullable: true),
                    PackedForViabTestDate = table.Column<DateTime>(nullable: true),
                    ViabTestResult = table.Column<int>(nullable: false),
                    ViabTestResultDate = table.Column<DateTime>(nullable: true),
                    QtyWdrLTS = table.Column<float>(nullable: false),
                    QtyWdrFC = table.Column<float>(nullable: false),
                    QtyWdrSVB = table.Column<float>(nullable: false),
                    QtyWdrDist = table.Column<float>(nullable: false),
                    DistId = table.Column<int>(nullable: false),
                    SampleSentForDist = table.Column<bool>(nullable: true),
                    NewId = table.Column<int>(nullable: false),
                    NewIdentifierType = table.Column<string>(nullable: true),
                    LocationInDryRoom = table.Column<string>(nullable: true),
                    PackedForMTS = table.Column<bool>(nullable: true),
                    DatestorageInMTS = table.Column<DateTime>(nullable: true),
                    DateSentToHealthUnit = table.Column<DateTime>(nullable: true),
                    DateGetFromHealthUnit = table.Column<DateTime>(nullable: true),
                    InGG = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowDetails_IdentifierType_IdentifierTypeId",
                        column: x => x.IdentifierTypeId,
                        principalTable: "IdentifierType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkflowDetails_Workflows_WorkflowId",
                        column: x => x.WorkflowId,
                        principalTable: "Workflows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDetails_IdentifierTypeId",
                table: "WorkflowDetails",
                column: "IdentifierTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDetails_WorkflowId",
                table: "WorkflowDetails",
                column: "WorkflowId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflows_WorkflowTypeId",
                table: "Workflows",
                column: "WorkflowTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflows_workflowStateId",
                table: "Workflows",
                column: "workflowStateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkflowActions");

            migrationBuilder.DropTable(
                name: "WorkflowDetails");

            migrationBuilder.DropTable(
                name: "IdentifierType");

            migrationBuilder.DropTable(
                name: "Workflows");

            migrationBuilder.DropTable(
                name: "WorkflowTypes");

            migrationBuilder.DropTable(
                name: "WorkflowState");
        }
    }
}
