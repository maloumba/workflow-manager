﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfManager.Entites
{
    class WorkflowDetailComparer : IEqualityComparer<WorkflowDetail>
    {
        public bool Equals(WorkflowDetail x, WorkflowDetail y)
        {
            if (x == null || y == null)
                return false;
            if (x.Workflow == null)
                return false;
            if (y.Workflow == null)
                return false;
            if (x.WorkflowId != y.WorkflowId)
                return false;

            return x.MaId == y.MaId;
             
        }

        public int GetHashCode(WorkflowDetail obj)
        {
            throw new NotImplementedException();
        }
    }
}
