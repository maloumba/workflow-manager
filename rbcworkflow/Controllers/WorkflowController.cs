﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rbcEntities;
using rbcworkflow.Helpers;
using rbcworkflow.ServiceModels;
using rbcworkflowCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rbcworkflow.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkflowController : Controller
    {
        IUnitOfWork _unitofwork;
        public WorkflowController(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("getworkflow")]
        public IEnumerable<WorkflowClient> Get()
        {
            var workflows =ServiceModelHelper.FormatWorkflow(_unitofwork.WorkflowRepository.GetList(t => t.Id > 0,w=>w.WorkflowState,y=>y.WorkflowType).ToList());
            return workflows;
        }

        // GET api/<controller>/5
        [HttpGet("getworkflow/{id}")]
        public ActionResult<Workflow> Get(int id)
        {
            var workflow = _unitofwork.WorkflowRepository.GetSingle(i => i.Id == id);
            if (!(workflow is Workflow))
            {
                return NotFound();
            }
            return workflow;
        }


        [HttpGet("getworkflowtypes")]
        public IEnumerable<WorkflowType> getworkflowtypes()
        {
            var workflows = _unitofwork.WorkflowTypeRepository.GetList(t => t.Id > 0).ToList();
            return workflows;
        }

       

        // POST api/<controller>
        [HttpPost]
        public ActionResult Post(Workflow value)
        {
            _unitofwork.WorkflowRepository.Add(value);
            _unitofwork.Complete();
            return CreatedAtAction(nameof(Get), new { id = value.Id }, value);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, Workflow value)
        {
            if (id != value.Id) { return BadRequest(); }
            _unitofwork.WorkflowRepository.Update(value);
            _unitofwork.Complete();
            return NoContent();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var workflow = _unitofwork.WorkflowRepository.GetSingle(w => w.Id == id);
            _unitofwork.WorkflowRepository.Remove(workflow);
            return NoContent();
        }
       
    }
}
