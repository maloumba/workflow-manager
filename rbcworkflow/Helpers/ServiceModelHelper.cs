﻿using rbcEntities;
using rbcworkflow.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rbcworkflow.Helpers
{
    public static class ServiceModelHelper
    {
        /// <summary>
        /// Convert WorkFlow class to wokflowClient
        /// </summary>
        /// <param name="workflowList"></param>
        /// <returns></returns>
        public static List<WorkflowClient> FormatWorkflow(List<Workflow> workflowList)
        {
            List<WorkflowClient> wkfclients = new List<WorkflowClient>();

            foreach (var item in workflowList)
            {
                wkfclients.Add(new WorkflowClient(item));
               
            }
            return wkfclients;
        }
    }
}
