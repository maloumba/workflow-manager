﻿using DataAccess;
using rbcEntities;

namespace rbcworkflowCore
{
    public interface IUnitOfWork
    {
         IGenericRepository<Workflow> WorkflowRepository { get; }
         IGenericRepository<ActionbyWorkflowType> ActionbyWorkflowTypeRepository { get; }
         IGenericRepository<IdentifierType> IdentifierTypeRepository { get; }
         IGenericRepository<WorkflowAction> WorkflowActionRepository { get; }
         IGenericRepository<WorkflowDetail> WorkflowDetailRepository { get; }
         IGenericRepository<WorkflowState> WorkflowStateRepository { get; }
         IGenericRepository<WorkflowType> WorkflowTypeRepository { get; }
         void Complete();
    }
}
