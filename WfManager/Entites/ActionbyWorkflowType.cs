﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfManager.Entites
{
   public class ActionbyWorkflowType
    {
        [Key]
        [Column(Order = 1)]
        public int WorkflowTypeId { get; set; }
        public WorkflowType WorflowType { get; set; }
        [Key]
        [Column(Order = 2)]
        public int WorkflowActionId { get; set; }
        public WorkflowAction WorkflowAction { get; set; }
    }
}
