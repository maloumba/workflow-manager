﻿namespace rbcEntities
{
    /// <summary>
    /// Define the state of a workflow(Initiated,FInished,etc)
    /// </summary>
    public class WorkflowState
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}