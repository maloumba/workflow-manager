﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class MakenullfieldsonWorkflowDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ViabTestResult",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrSVB",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrMTS",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrLTS",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrFC",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrDist",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<int>(
                name: "NewId",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<float>(
                name: "CurrentQty",
                table: "WorkflowDetails",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AddColumn<int>(
                name: "WorkflowId",
                table: "WorkflowActions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowActions_WorkflowId",
                table: "WorkflowActions",
                column: "WorkflowId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkflowActions_Workflows_WorkflowId",
                table: "WorkflowActions",
                column: "WorkflowId",
                principalTable: "Workflows",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkflowActions_Workflows_WorkflowId",
                table: "WorkflowActions");

            migrationBuilder.DropIndex(
                name: "IX_WorkflowActions_WorkflowId",
                table: "WorkflowActions");

            migrationBuilder.DropColumn(
                name: "WorkflowId",
                table: "WorkflowActions");

            migrationBuilder.AlterColumn<int>(
                name: "ViabTestResult",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrSVB",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrMTS",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrLTS",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrFC",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "QtyWdrDist",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NewId",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "CurrentQty",
                table: "WorkflowDetails",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);
        }
    }
}
