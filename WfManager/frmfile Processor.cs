﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfManager
{
    public partial class frmfile_Processor : Form
    {
        Initiator initiator = DependencyInjector.Retrieve<Initiator>();
        public frmfile_Processor()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            int fileCount = 0;
            //Move files from the "root source directory" to a "subRepertory" with Accenumb as "Repertory name"
            //in the Dest repertory main repertory.
            foreach (var item in Directory.EnumerateFiles(txtSourcePath.Text))
            {
                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 5);
                Console.WriteLine(fileName);
                var accession = initiator._businessClass.AccessionRepository.GetSingle(a => a.Accename.Equals(fileName) || a.AccessionId.Equals(fileName));
                if (accession == null)
                    continue;
                if (!Directory.Exists(txtDestinationPath.Text + $"\\{accession.AccessionId}"))
                    Directory.CreateDirectory(txtDestinationPath.Text + $"\\{accession.AccessionId}");
                File.Copy(item, txtDestinationPath.Text + $"\\{accession.AccessionId}\\{fileName}.JPG");
                fileCount++;  
            }
           
            foreach (var item in Directory.EnumerateDirectories(txtSourcePath.Text))
            {

                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator+1, (item.Length - lastIndexofSeparator)-1);
                var accession = initiator._businessClass.AccessionRepository.GetSingle(a => a.Accename.Equals(fileName) || a.AccessionId.Equals(fileName));
                if (accession == null)
                    continue;
                if (!Directory.Exists(txtDestinationPath.Text + $"\\{accession.AccessionId}"))
                    Directory.Move(item, txtDestinationPath.Text + $"\\{accession.AccessionId}");
                fileCount++;
                Console.WriteLine(item);
            }
            Console.WriteLine(fileCount);
        }
    }
}
