﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicturesManager
{
    public partial class frmpictureViewer : Form
    {
        List<string> _currentpictureList;
        int pictureIndex = 0;
        public frmpictureViewer(List<string> pictureList)
        {
            InitializeComponent();
            this._currentpictureList = pictureList;
            int i = 0;
            foreach (var item in _currentpictureList)
            {
                imageList1.Images.Add(i.ToString(),Image.FromFile( _currentpictureList[i]));
            }
        }

        private void frmpictureViewer_Load(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = _currentpictureList[pictureIndex];
            pictureBox1.Invalidate();
            labelIndex.Text = $"Showing {pictureIndex+1} of {_currentpictureList.Count} picture(s).";
            //showCurrentPictDetails();
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            pictureIndex--;
            pictureIndex = (pictureIndex > 0) ? (pictureIndex--) : _currentpictureList.Count - 1;
            showCurrentPictDetails();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            pictureIndex++;
            pictureIndex = (pictureIndex < _currentpictureList.Count) ? (pictureIndex++) : pictureIndex = 0;
            showCurrentPictDetails();        
        }


        void showCurrentPictDetails()
        {
            pictureBox1.ImageLocation = _currentpictureList[pictureIndex];
            string pictSize = pictureBox1.Image.Size.ToString();
            //pictureBox1.Image = imageList1.Images[pictureIndex];
            labelIndex.Text = $"Showing {pictureIndex+1} of {_currentpictureList.Count} picture(s).";
            labelSize.Text = $"Size:{pictSize}";
            LabelName.Text = getFileName(pictureBox1.ImageLocation);
        }


        string getFileName(string path)
        {
            int lastIndexofSeparator = path.LastIndexOf("\\");
            string fileName = path.Substring(lastIndexofSeparator + 1, (path.Length - lastIndexofSeparator) - 5);
            return fileName;
        }
    }
}
