namespace WfManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateWorkflowDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkflowDetails", "CleaningDate", c => c.DateTime());
            AddColumn("dbo.WorkflowDetails", "Datestorage", c => c.DateTime());
            AlterColumn("dbo.WorkflowDetails", "NewId", c => c.String());
            DropColumn("dbo.WorkflowDetails", "CleaningStartDate");
            DropColumn("dbo.WorkflowDetails", "CleaningEndDate");
            DropColumn("dbo.WorkflowDetails", "DatestorageInMTS");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkflowDetails", "DatestorageInMTS", c => c.DateTime());
            AddColumn("dbo.WorkflowDetails", "CleaningEndDate", c => c.DateTime());
            AddColumn("dbo.WorkflowDetails", "CleaningStartDate", c => c.DateTime());
            AlterColumn("dbo.WorkflowDetails", "NewId", c => c.Int());
            DropColumn("dbo.WorkflowDetails", "Datestorage");
            DropColumn("dbo.WorkflowDetails", "CleaningDate");
        }
    }
}
