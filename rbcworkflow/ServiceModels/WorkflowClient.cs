﻿using rbcEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rbcworkflow.ServiceModels
{
    public class WorkflowClient
    {
        public int Id { get; set; }
        public string WorkflowState { get; set; }
        public int WorkflowStateId { get; set; }
        public int WorkflowTypeId { get; set; }
        public string WorkflowType { get; set; }
        public int Year { get; set; }
        public WorkflowClient(Workflow item)
        {
            Id = item.Id;
            WorkflowState = item.WorkflowState.Name;
            WorkflowStateId = item.workflowStateId;
            WorkflowType = item.WorkflowType.Name;
            WorkflowTypeId = item.WorkflowTypeId;
            Year = item.Year;
        }
    }
}
