﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using rbcEntities;
using rbcworkflowCore;

namespace rbcworkflow.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionsController : ControllerBase
    {
        IUnitOfWork _unitofwork;
       
        public ActionsController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }

        [HttpPost]
        public ActionResult Create(WorkflowAction value)
        {
            _unitofwork.WorkflowActionRepository.Add(value);
            _unitofwork.Complete();
            return CreatedAtAction(nameof(Get), new { id = value.Id }, value);
        }

        [HttpGet]
        public IEnumerable<WorkflowAction> Get()
        {
            var workflowaction = _unitofwork.WorkflowActionRepository.GetAll().ToList();
            return workflowaction;
        }

        [HttpGet]
        public ActionResult<WorkflowAction> Get(int id)
        {
            var workflowaction = _unitofwork.WorkflowActionRepository.GetSingle(i => i.Id == id);
            if (!(workflowaction is WorkflowAction))
            {
                return NotFound();
            }
            return workflowaction;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, WorkflowAction value)
        {
            if (id != value.Id) { return BadRequest(); }
            _unitofwork.WorkflowActionRepository.Update(value);
            _unitofwork.Complete();
            return NoContent();
        }


        // PUT api/<controller>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id, WorkflowAction value)
        {
            var workflow = _unitofwork.WorkflowActionRepository.GetSingle(w => w.Id == id);
            _unitofwork.WorkflowActionRepository.Remove(workflow);
            return NoContent();
        }

    }
}