﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rbcEntities
{
   public class rbcActionBase
    {
        /// <summary>
        /// Date de creation de l'action
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// Identite de l'initiateur
        /// </summary>
        public string CreateBy { get; set; }
        /// <summary>
        /// Date de mise a jour
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// Identitte de l'initateur de la mise a jour
        /// </summary>
        public string UpdateBy { get; set; }
        public string InitiatedBy { get; set; }
        public string TerminatedBy { get; set; }


    }
}
