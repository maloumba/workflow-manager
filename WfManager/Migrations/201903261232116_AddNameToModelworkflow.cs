namespace WfManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameToModelworkflow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Workflows", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Workflows", "Name");
        }
    }
}
