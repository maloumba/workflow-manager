﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WfManager.Entites;

namespace WfManager
{
    public partial class FrmAddDetails : Form
    {
        Workflow _currentworkflow;
        Initiator initiator = DependencyInjector.Retrieve<Initiator>();
        List<WorkflowDetail> _currentNewDetailList;
        public FrmAddDetails(Workflow workflow)
        {
            InitializeComponent();
            _currentworkflow = workflow;
            _currentNewDetailList = new List<WorkflowDetail>();
        }

        private void gridNewSample_DragDrop(object sender, DragEventArgs e)
        {
            gridNewSample.DataBindings.Clear();

            if (e.Data.GetDataPresent(DataFormats.CommaSeparatedValue))
            {
                var Alldata = (string)e.Data.GetData(DataFormats.Text);
                string[] data = Alldata.Split('\r', '\n');
                string[] columns = data[0].Split('\t');//On parse la première ligne utilisant la tabulation
                AddColumns(columns);//On cree les columns en utilisant la premiere ligne
                for (int i = 1; i < data.Length; i++)
                {
                    try
                    {
                        //  
                        string[] ligne = data[i].Split('\t');//On parse la ligne utilisant la tabulation

                        if (!(ligne.Where(l => l.Length > 0).Count() > 0))//elimination des lignes vides
                            continue;
                        gridNewSample.Rows.Add();
                        for (int j = 0; j < ligne.Length; j++)
                        {
                            gridNewSample.Rows[gridNewSample.Rows.Count - 1].Cells[j].Value = ligne[j] == null ? "" : ligne[j];//
                        }
                        gridNewSample.Rows[gridNewSample.Rows.Count - 1].Cells["WorkflowId"].Value = _currentworkflow.Id;
                    }
                    catch (InvalidOperationException ex)
                    {
                        MessageBox.Show(ex.Message);
                        break;
                    }
                }
            }
        }

        private void AddColumns(string[] columns)
        {
            //WorkflowDetail obj = new WorkflowDetail();
            //Type type = obj.GetType();
            //List<string> listworkFlowFields = new List<string>();
            //foreach (var f in type.GetFields(System.Reflection.BindingFlags.Public))
            //{
            //    listworkFlowFields.Add(f.Name);
            //}
            foreach (var item in columns)
            {
                if(!gridNewSample.Columns.Contains(item.ToString()))
                gridNewSample.Columns.Add(item.ToString(), item.ToString());
            }
        }

        private void gridNewSample_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.CommaSeparatedValue))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            List<WorkflowDetail> detailsList = new List<WorkflowDetail>();
            WorkflowDetail oneDetail;
            for (int i = 0; i < gridNewSample.RowCount-1; i++)
            {
                oneDetail = new WorkflowDetail
                {
                    WorkflowId = int.Parse(gridNewSample.Rows[i].Cells["WorkflowId"].Value.ToString()),
                    MaId = gridNewSample.Rows[i].Cells["MaId"].Value.ToString(),
                    IdentifierTypeId = int.Parse(gridNewSample.Rows[i].Cells["IdentifierTypeId"].Value.ToString()),
                    Location = gridNewSample.Rows[i].Cells["Location"].Value.ToString(),
                    Name = gridNewSample.Rows[i].Cells["M_Name"].Value.ToString()
                };
           
                detailsList.Add(oneDetail);
            }
            try
            {
                initiator._businessClass.WorkflowDetailRepository.Add(detailsList.ToArray());
                initiator._businessClass.Complete();
                MessageBox.Show("List of details added!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmAddDetails_Load(object sender, EventArgs e)
        {
            FillIdentifierTypes();

        }

        private void FillLocations(string InventoryId)
        {
         
        }

        private void FillIdentifierTypes()
        {
            var identifiers = initiator._businessClass.IdentifierTypeRepository.GetAll();
            comboIdentifierType.DataSource = identifiers;
        }

        private void txtMaterialId_TextChanged(object sender, EventArgs e)
        {
            ShowAccession();

        }

        private void ShowAccession()
        {
            
            IdentifierType identType = comboIdentifierType.SelectedItem as IdentifierType;
            switch (identType.Id)
            {
                case 1:
                    if (!int.TryParse(txtMaterialId.Text, out int id))
                        return;
                    var material = initiator._businessClass.InventoryRepository.GetSingle(i => i.Id == id);
                    var accession = initiator.
                        _businessClass
                        .AccessionRepository
                        .GetSingle(a => a
                        .AccessionId
                        .Equals(material.Accenumb));
                    labMaterialName.Text = accession.Accename;
                    comboLocations.Items.Clear();
                    comboLocations.Items.Add(material.Position);
                    break;
                case 2:
                    var acc = initiator.
                        _businessClass
                        .AccessionRepository
                        .GetSingle(a => a
                        .AccessionId
                        .Equals(txtMaterialId.Text));
                    if (acc is null)
                        return;
                    labMaterialName.Text = acc.Accename;
                    var positions = initiator._businessClass.InventoryRepository.GetList(i=>i.Accenumb!=null&& i.Accenumb.Equals(acc.AccessionId));
                    comboLocations.Items.Clear();
                    foreach (var item in positions)
                    {
                        comboLocations.Items.Add(item.Position);
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this._currentNewDetailList.Add(new WorkflowDetail
            {
                WorkflowId = _currentworkflow.Id,
                MaId = txtMaterialId.Text,
                IdentifierTypeId = (comboIdentifierType.SelectedItem as IdentifierType).Id,
                Location = comboLocations.SelectedItem == null ? comboLocations.Items[0].ToString() : comboLocations.SelectedItem.ToString(),
                Name=labMaterialName.Text
            });
            gridNewSample.Rows.Clear();
            foreach (var item in _currentNewDetailList)
            {
                gridNewSample.Rows.Add(_currentworkflow.Id, item.MaId, item.IdentifierTypeId, item.Location, item.Name);
                
            };
           
        }

        private void comboIdentifierType_SelectedIndexChanged(object sender, EventArgs e)
        {
           ShowAccession();
        }

        private void btnRemoveDuplicates_Click(object sender, EventArgs e)
        {
           // _currentNewDetailList = _currentNewDetailList.Distinct(new WorkflowDetailComparer<WorkflowDetail>());
        }
    }

}
