﻿namespace WfManager
{
    partial class FrmAddDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.labMaterialName = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.comboLocations = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboIdentifierType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaterialId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridAllSamples = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Actions = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnValidate = new System.Windows.Forms.Button();
            this.btnRemoveDuplicates = new System.Windows.Forms.Button();
            this.gridNewSample = new System.Windows.Forms.DataGridView();
            this.WorkflowId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdentifierTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAllSamples)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.Actions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNewSample)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.labMaterialName);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.comboLocations);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.comboIdentifierType);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtMaterialId);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(0, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(833, 562);
            this.panel2.TabIndex = 4;
            // 
            // labMaterialName
            // 
            this.labMaterialName.AutoSize = true;
            this.labMaterialName.Location = new System.Drawing.Point(385, 14);
            this.labMaterialName.Name = "labMaterialName";
            this.labMaterialName.Size = new System.Drawing.Size(17, 13);
            this.labMaterialName.TabIndex = 18;
            this.labMaterialName.Text = "//";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(105, 101);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(112, 34);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // comboLocations
            // 
            this.comboLocations.FormattingEnabled = true;
            this.comboLocations.Location = new System.Drawing.Point(105, 65);
            this.comboLocations.Name = "comboLocations";
            this.comboLocations.Size = new System.Drawing.Size(146, 21);
            this.comboLocations.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Location:";
            // 
            // comboIdentifierType
            // 
            this.comboIdentifierType.FormattingEnabled = true;
            this.comboIdentifierType.Location = new System.Drawing.Point(105, 38);
            this.comboIdentifierType.Name = "comboIdentifierType";
            this.comboIdentifierType.Size = new System.Drawing.Size(146, 21);
            this.comboIdentifierType.TabIndex = 14;
            this.comboIdentifierType.SelectedIndexChanged += new System.EventHandler(this.comboIdentifierType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "Identifier type:";
            // 
            // txtMaterialId
            // 
            this.txtMaterialId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaterialId.Location = new System.Drawing.Point(105, 8);
            this.txtMaterialId.Name = "txtMaterialId";
            this.txtMaterialId.Size = new System.Drawing.Size(274, 24);
            this.txtMaterialId.TabIndex = 12;
            this.txtMaterialId.TextChanged += new System.EventHandler(this.txtMaterialId_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Material Id:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridAllSamples);
            this.groupBox3.Location = new System.Drawing.Point(1, 330);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(831, 227);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "workflow all samples list";
            // 
            // gridAllSamples
            // 
            this.gridAllSamples.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridAllSamples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAllSamples.Location = new System.Drawing.Point(11, 38);
            this.gridAllSamples.Name = "gridAllSamples";
            this.gridAllSamples.Size = new System.Drawing.Size(809, 178);
            this.gridAllSamples.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Actions);
            this.groupBox1.Controls.Add(this.gridNewSample);
            this.groupBox1.Location = new System.Drawing.Point(1, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(831, 183);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New sample\'s list";
            // 
            // Actions
            // 
            this.Actions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Actions.Controls.Add(this.button2);
            this.Actions.Controls.Add(this.btnValidate);
            this.Actions.Controls.Add(this.btnRemoveDuplicates);
            this.Actions.Location = new System.Drawing.Point(658, 19);
            this.Actions.Name = "Actions";
            this.Actions.Size = new System.Drawing.Size(167, 174);
            this.Actions.TabIndex = 1;
            this.Actions.TabStop = false;
            this.Actions.Text = "Action";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Cooper Black", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(18, 119);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 39);
            this.button2.TabIndex = 2;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnValidate
            // 
            this.btnValidate.Font = new System.Drawing.Font("Cooper Black", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.Location = new System.Drawing.Point(18, 74);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(116, 39);
            this.btnValidate.TabIndex = 1;
            this.btnValidate.Text = "Validate";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // btnRemoveDuplicates
            // 
            this.btnRemoveDuplicates.Font = new System.Drawing.Font("Cooper Black", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveDuplicates.Location = new System.Drawing.Point(18, 29);
            this.btnRemoveDuplicates.Name = "btnRemoveDuplicates";
            this.btnRemoveDuplicates.Size = new System.Drawing.Size(116, 39);
            this.btnRemoveDuplicates.TabIndex = 0;
            this.btnRemoveDuplicates.Text = "Remove\r\nDuplicates\r\n";
            this.btnRemoveDuplicates.UseVisualStyleBackColor = true;
            this.btnRemoveDuplicates.Click += new System.EventHandler(this.btnRemoveDuplicates_Click);
            // 
            // gridNewSample
            // 
            this.gridNewSample.AllowDrop = true;
            this.gridNewSample.AllowUserToAddRows = false;
            this.gridNewSample.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridNewSample.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridNewSample.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WorkflowId,
            this.MaId,
            this.IdentifierTypeId,
            this.Location,
            this.M_Name});
            this.gridNewSample.Location = new System.Drawing.Point(11, 19);
            this.gridNewSample.Name = "gridNewSample";
            this.gridNewSample.Size = new System.Drawing.Size(641, 158);
            this.gridNewSample.TabIndex = 0;
            this.gridNewSample.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridNewSample_DragDrop);
            this.gridNewSample.DragEnter += new System.Windows.Forms.DragEventHandler(this.gridNewSample_DragEnter);
            // 
            // WorkflowId
            // 
            this.WorkflowId.HeaderText = "WorkflowId";
            this.WorkflowId.Name = "WorkflowId";
            this.WorkflowId.ReadOnly = true;
            // 
            // MaId
            // 
            this.MaId.HeaderText = "MaId";
            this.MaId.Name = "MaId";
            // 
            // IdentifierTypeId
            // 
            this.IdentifierTypeId.HeaderText = "IdentifierTypeId";
            this.IdentifierTypeId.Name = "IdentifierTypeId";
            // 
            // Location
            // 
            this.Location.HeaderText = "Location";
            this.Location.Name = "Location";
            // 
            // M_Name
            // 
            this.M_Name.HeaderText = "Name";
            this.M_Name.Name = "M_Name";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(840, 37);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add workflow data";
            // 
            // FrmAddDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 617);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FrmAddDetails";
            this.Text = "FrmAddDetails";
            this.Load += new System.EventHandler(this.FrmAddDetails_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAllSamples)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.Actions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridNewSample)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gridNewSample;
        private System.Windows.Forms.GroupBox Actions;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Button btnRemoveDuplicates;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView gridAllSamples;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkflowId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdentifierTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Location;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_Name;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox comboLocations;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboIdentifierType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMaterialId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labMaterialName;
    }
}