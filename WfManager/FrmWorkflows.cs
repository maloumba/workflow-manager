﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WfManager.Entites;

namespace WfManager
{
    public partial class FrmWorkflows : Form
    {
        Initiator initiator = DependencyInjector.Retrieve<Initiator>();
        public FrmWorkflows()
        {
            InitializeComponent();
        }

        private void FrmWorkflows_Load(object sender, EventArgs e)
        {
            FillGridWorkflows();
            fillcomboStates();
        }

        private void FillGridWorkflows()
        {
            gridWorkflows.DataSource = initiator._businessClass.WorkflowRepository.GetList(w => w.Id >= 1, w => w.WorkflowType, w => w.WorkflowState);
        }

        private void fillcomboStates()
        {
            comboBostates.DataSource = initiator._businessClass.WorkflowStateRepository.GetAll();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            gridWorkflows.DataSource = initiator._businessClass.WorkflowRepository
                .GetList(w => w.Name.ToLower().Contains(txtName.Text), 
                t => t.WorkflowType, 
                s => s.WorkflowState);
        }

        private void btnSearchByYear_Click(object sender, EventArgs e)
        {
            int year = int.Parse(ComboYear.SelectedItem.ToString());
            gridWorkflows.DataSource = initiator._businessClass.WorkflowRepository
                .GetList(w => w.Year == year,
                t => t.WorkflowType, 
                s => s.WorkflowState);
        }

        private void btnSearchByStates_Click(object sender, EventArgs e)
        {
            var stateId =( comboBostates.SelectedItem as WorkflowState).Id;
            gridWorkflows.DataSource = initiator._businessClass.WorkflowRepository.
                GetList(w => w.workflowStateId == stateId,
                t => t.WorkflowType,
                s => s.WorkflowState);
        }

        private void btnAddnew_Click(object sender, EventArgs e)
        {
            AddWorkflow objworkflow = new AddWorkflow();
            if(objworkflow.ShowDialog()==DialogResult.OK)
            {
                FillGridWorkflows();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void cancelWorkflowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridWorkflows.CurrentRow.DataBoundItem as Workflow == null)
                return;
            frmEditworkflowStates objstate = new frmEditworkflowStates();
            if(objstate.ShowDialog()==DialogResult.OK)
            {
                var workflowId = (gridWorkflows.CurrentRow.DataBoundItem as Workflow).Id;
                var workflowToupdate=  initiator._businessClass.WorkflowRepository.GetSingle(w => w.Id == workflowId);
                try
                {
                    initiator._businessClass.WorkflowRepository.Update(workflowToupdate);
                    initiator._businessClass.Complete();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void addWorkflowDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void addDetailsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var workflow = gridWorkflows.CurrentRow.DataBoundItem as Workflow;
            if (!(workflow is Workflow))
                return;
            FrmAddDetails frmdetailList = new FrmAddDetails(workflow);
            frmdetailList.ShowDialog();
        }

        private void gridWorkflows_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
