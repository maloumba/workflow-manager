﻿using DataAccess;
using rbcEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace rbcworkflowCore
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RbcDbContext _dbcontext;
        public UnitOfWork(RbcDbContext objContext )
        {
            _dbcontext = objContext;
            IdentifierTypeRepository = new GenericRepository<IdentifierType>(objContext);
            WorkflowRepository = new GenericRepository<Workflow>(objContext);
            ActionbyWorkflowTypeRepository = new GenericRepository<ActionbyWorkflowType>(objContext);
            WorkflowActionRepository = new GenericRepository<WorkflowAction>(objContext);
            WorkflowDetailRepository = new GenericRepository<WorkflowDetail>(objContext);
            WorkflowStateRepository = new GenericRepository<WorkflowState>(objContext);
            WorkflowTypeRepository = new GenericRepository<WorkflowType>(objContext);
        }

        public IGenericRepository<Workflow> WorkflowRepository { get; private set; }

        public IGenericRepository<ActionbyWorkflowType> ActionbyWorkflowTypeRepository { get; private set; }

        public IGenericRepository<IdentifierType> IdentifierTypeRepository { get; private set; }

        public IGenericRepository<WorkflowAction> WorkflowActionRepository { get; private set; }

        public IGenericRepository<WorkflowDetail> WorkflowDetailRepository { get; private set; }

        public IGenericRepository<WorkflowState> WorkflowStateRepository { get; private set; }

        public IGenericRepository<WorkflowType> WorkflowTypeRepository { get; private set; }

       
        public void Complete()
        {
            _dbcontext.SaveChanges();
        }
    }
}
