﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfManager.DataAccess;
using WfManager.Entites;

namespace WfManager.Core
{
  public interface IUnitOfWork
    {
        IGenericRepository<Workflow> WorkflowRepository { get; }
        IGenericRepository<ActionbyWorkflowType> ActionbyWorkflowTypeRepository { get; }
        IGenericRepository<IdentifierType> IdentifierTypeRepository { get; }
        IGenericRepository<WorkflowAction> WorkflowActionRepository { get; }
        IGenericRepository<WorkflowDetail> WorkflowDetailRepository { get; }
        IGenericRepository<WorkflowState> WorkflowStateRepository { get; }
        IGenericRepository<WorkflowType> WorkflowTypeRepository { get; }
        IGenericRepository<gg_inventory> InventoryRepository { get; }
        IGenericRepository<Pas_Accession> AccessionRepository { get; }
        void Complete();
    }
}
