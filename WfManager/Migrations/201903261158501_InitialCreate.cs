namespace WfManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionbyWorkflowTypes",
                c => new
                    {
                        WorkflowTypeId = c.Int(nullable: false),
                        WorkflowActionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkflowTypeId, t.WorkflowActionId })
                .ForeignKey("dbo.WorkflowTypes", t => t.WorkflowTypeId, cascadeDelete: true)
                .ForeignKey("dbo.WorkflowActions", t => t.WorkflowActionId, cascadeDelete: true)
                .Index(t => t.WorkflowTypeId)
                .Index(t => t.WorkflowActionId);
            
            CreateTable(
                "dbo.WorkflowTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowActions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Workflow_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Workflows", t => t.Workflow_Id)
                .Index(t => t.Workflow_Id);
            
            CreateTable(
                "dbo.WorkflowDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkflowId = c.Int(nullable: false),
                        MaId = c.String(),
                        IdentifierTypeId = c.Int(nullable: false),
                        Location = c.String(),
                        Name = c.String(),
                        QtyWdrMTS = c.Single(),
                        CurrentQty = c.Single(),
                        HeatingstartDate = c.DateTime(),
                        HeatingEndDate = c.DateTime(),
                        Pre_germinationDate = c.DateTime(),
                        NurseryDate = c.DateTime(),
                        SentToFieldDate = c.DateTime(),
                        SowingDate = c.DateTime(),
                        SowedBy = c.String(),
                        HarvestDate = c.DateTime(),
                        HarvestBy = c.String(),
                        WinnowingDate = c.DateTime(),
                        PreDryDate = c.DateTime(),
                        StartDryingDate = c.DateTime(),
                        EndDryingDate = c.DateTime(),
                        WADried = c.Boolean(nullable: false),
                        PackedForCleaningDate = c.DateTime(),
                        CleaningStartDate = c.DateTime(),
                        CleaningEndDate = c.DateTime(),
                        CleanedBy = c.String(),
                        PackedForViabTestDate = c.DateTime(),
                        ViabTestResult = c.Int(),
                        ViabTestResultDate = c.DateTime(),
                        QtyWdrLTS = c.Single(),
                        QtyWdrFC = c.Single(),
                        QtyWdrSVB = c.Single(),
                        QtyWdrDist = c.Single(),
                        DistId = c.Int(nullable: false),
                        SampleSentForDist = c.Boolean(),
                        NewId = c.Int(),
                        NewIdentifierType = c.String(),
                        LocationInDryRoom = c.String(),
                        PackedForMTS = c.Boolean(),
                        DatestorageInMTS = c.DateTime(),
                        DateSentToHealthUnit = c.DateTime(),
                        DateGetFromHealthUnit = c.DateTime(),
                        InGG = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IdentifierTypes", t => t.IdentifierTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Workflows", t => t.WorkflowId, cascadeDelete: true)
                .Index(t => t.WorkflowId)
                .Index(t => t.IdentifierTypeId);
            
            CreateTable(
                "dbo.IdentifierTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Workflows",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        workflowStateId = c.Int(nullable: false),
                        WorkflowTypeId = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkflowStates", t => t.workflowStateId, cascadeDelete: true)
                .ForeignKey("dbo.WorkflowTypes", t => t.WorkflowTypeId, cascadeDelete: true)
                .Index(t => t.workflowStateId)
                .Index(t => t.WorkflowTypeId);
            
            CreateTable(
                "dbo.WorkflowStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkflowDetails", "WorkflowId", "dbo.Workflows");
            DropForeignKey("dbo.Workflows", "WorkflowTypeId", "dbo.WorkflowTypes");
            DropForeignKey("dbo.Workflows", "workflowStateId", "dbo.WorkflowStates");
            DropForeignKey("dbo.WorkflowActions", "Workflow_Id", "dbo.Workflows");
            DropForeignKey("dbo.WorkflowDetails", "IdentifierTypeId", "dbo.IdentifierTypes");
            DropForeignKey("dbo.ActionbyWorkflowTypes", "WorkflowActionId", "dbo.WorkflowActions");
            DropForeignKey("dbo.ActionbyWorkflowTypes", "WorkflowTypeId", "dbo.WorkflowTypes");
            DropIndex("dbo.Workflows", new[] { "WorkflowTypeId" });
            DropIndex("dbo.Workflows", new[] { "workflowStateId" });
            DropIndex("dbo.WorkflowDetails", new[] { "IdentifierTypeId" });
            DropIndex("dbo.WorkflowDetails", new[] { "WorkflowId" });
            DropIndex("dbo.WorkflowActions", new[] { "Workflow_Id" });
            DropIndex("dbo.ActionbyWorkflowTypes", new[] { "WorkflowActionId" });
            DropIndex("dbo.ActionbyWorkflowTypes", new[] { "WorkflowTypeId" });
            DropTable("dbo.WorkflowStates");
            DropTable("dbo.Workflows");
            DropTable("dbo.IdentifierTypes");
            DropTable("dbo.WorkflowDetails");
            DropTable("dbo.WorkflowActions");
            DropTable("dbo.WorkflowTypes");
            DropTable("dbo.ActionbyWorkflowTypes");
        }
    }
}
