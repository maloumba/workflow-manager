﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WfManager
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            Initiator initiator;
        }

        private void workflowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddWorkflow frmAddWorkflow = new AddWorkflow();
            frmAddWorkflow.MdiParent = this;
            frmAddWorkflow.Show();

               
        }

        private void btnworkflow_Click(object sender, EventArgs e)
        {
            FrmWorkflows objworkflows = new FrmWorkflows();
            objworkflows.MdiParent = this;
            objworkflows.Show();
        }

        private void pictureDatabaseProcessingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmfile_Processor processor = new frmfile_Processor();
            processor.ShowDialog();
        }
    }
}
