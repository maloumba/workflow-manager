﻿using System.Collections.Generic;

namespace rbcEntities
{
    public class WorkflowAction
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}