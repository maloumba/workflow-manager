﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfManager.Entites
{
  public  class Workflow
    {
        public int Id { get; set; }
        public WorkflowState WorkflowState { get; set; }
        public int workflowStateId { get; set; }
        public int WorkflowTypeId { get; set; }
        public WorkflowType WorkflowType { get; set; }
        public int Year { get; set; }
        public string Name { get; set; }
        public ICollection<WorkflowAction> WorkflowDetails { get; set; }
    }
}
