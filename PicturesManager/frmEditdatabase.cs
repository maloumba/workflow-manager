﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicturesManager
{
    public partial class frmEditdatabase : Form
    {
        int _directories = 0;
        int _files = 0;
        public DataTable Database {
            get { return _dt; }
        }
        DataTable _dt;
        char databaseCSVChar;
        public frmEditdatabase(DataTable dt,char csvChar)
        {
            InitializeComponent();
            _dt = dt;
            PictureGrid.DataSource = _dt;
            _directories= dt.AsEnumerable().Count(r => r["TYPE"].Equals("Directory"));
            _files= dt.AsEnumerable().Count(r => r["TYPE"].ToString()!= "Directory");
            databaseCSVChar = csvChar;
        }

        private void frmEditdatabase_Load(object sender, EventArgs e)
        {
            labelfiles.Text = $"{_files} files founded";
            labelDirectories.Text = $"{_directories} directories founded";

        }
        string[] getRealAccename(string name)
        {            
            string symbol = comboBoxSymbol.SelectedItem.ToString();
            string[] tab = new string[2];
            try
            {
                if(name.Split(char.Parse(symbol)).Length > 1)
                {
                    tab[0] = name.Split(char.Parse(symbol))[0];
                    tab[1] = name.Split(char.Parse(symbol))[1];
                }
                else
                {
                    tab[0] = name.Split(char.Parse(symbol))[0];
                    tab[1] = txtDefaultDescription.Text;
                }                
                return tab;
            }
            catch (Exception)
            {
                tab[0] = name;
                tab[1] = "";
                return tab;           
            }           
        }
        private void btnMatch_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV file|*.csv*";
            
            if(dialog.ShowDialog()==DialogResult.OK)
            {
                Repository repository = new Repository(dialog.FileName);
                int i = 0;
                int matches = 0;
                for (;i<PictureGrid.Rows.Count-1;i++)
                {
                    var accename =getRealAccename(PictureGrid.Rows[i].Cells["ACCENAME"].Value.ToString());
                    var accenumb = repository.GetAcceNumb(accename[0],databaseCSVChar);
                    var description = accename[1];
                    if(accenumb!=null)
                    {
                        PictureGrid.Rows[i].Cells["ACCENUMB"].Value = accenumb;
                        PictureGrid.Rows[i].Cells["DESCRIPTION"].Value = description;
                        matches++;
                    }
                   
                }
                MessageBox.Show($"{matches} matches founded!");
            }
        }

        private void PictureGrid_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnshowPicture.Enabled = true;
            txtAccenumb.Enabled = true;
          
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = _dt.AsEnumerable().Where(r => r["ACCENAME"].ToString().Contains(txtSearch.Text)).CopyToDataTable();
                PictureGrid.DataSource = dt;
                labelsearchresult.Text = $"{dt.Rows.Count} picture(s) founded";
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
                labelsearchresult.Text = $"No picture(s) founded";
            }
           
        }

        private void linkReset_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PictureGrid.DataSource = null;
            PictureGrid.DataSource= _dt;
            labelsearchresult.Text = string.Empty;
            txtSearch.Text = string.Empty;
        }

        private void btnshowPicture_Click(object sender, EventArgs e)
        {
            List<string> pictList = new List<string>();
            foreach (var item in PictureGrid.SelectedRows)
            {
                var path = (item as DataGridViewRow).Cells["PATH"].Value.ToString() ;
                pictList.Add(path);
            }
            frmpictureViewer viewer = new frmpictureViewer(pictList);
            viewer.ShowDialog();
        }

        private void txtAccenumb_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAccenumb.Text))
            {
                btnBatchUpdate.Enabled = false;
                return;
            }
            btnBatchUpdate.Enabled = true;
        }

        private void btnBatchUpdate_Click(object sender, EventArgs e)
        {
            if (PictureGrid.SelectedRows == null)
                return;
            foreach (var item in PictureGrid.SelectedRows)
            {
                (item as DataGridViewRow).Cells["ACCENUMB"].Value = txtAccenumb.Text;
            }
            txtAccenumb.Text = string.Empty;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
