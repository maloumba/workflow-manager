﻿namespace PicturesManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelEnumeratedfiles = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboSymbols = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnenumerateFiles = new System.Windows.Forms.Button();
            this.btnenumerateDirectories = new System.Windows.Forms.Button();
            this.btnenumerateAll = new System.Windows.Forms.Button();
            this.btnEditfolder = new System.Windows.Forms.Button();
            this.txtResultGrid = new System.Windows.Forms.TextBox();
            this.txtDestPath = new System.Windows.Forms.TextBox();
            this.txtSourcePath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(927, 31);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::PicturesManager.Properties.Resources.bulkinsert;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(139, 28);
            this.toolStripButton1.Text = "Configure Database";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::PicturesManager.Properties.Resources.delete_sign;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(53, 28);
            this.toolStripButton2.Text = "Exit";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelEnumeratedfiles);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnEditfolder);
            this.panel1.Controls.Add(this.txtResultGrid);
            this.panel1.Controls.Add(this.txtDestPath);
            this.panel1.Controls.Add(this.txtSourcePath);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 620);
            this.panel1.TabIndex = 1;
            // 
            // labelEnumeratedfiles
            // 
            this.labelEnumeratedfiles.AutoSize = true;
            this.labelEnumeratedfiles.Location = new System.Drawing.Point(604, 559);
            this.labelEnumeratedfiles.Name = "labelEnumeratedfiles";
            this.labelEnumeratedfiles.Size = new System.Drawing.Size(17, 13);
            this.labelEnumeratedfiles.TabIndex = 17;
            this.labelEnumeratedfiles.Text = "//";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboSymbols);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(411, 162);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(430, 88);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application CSV file\'s database symbole";
            // 
            // comboSymbols
            // 
            this.comboSymbols.FormattingEnabled = true;
            this.comboSymbols.Items.AddRange(new object[] {
            "comma",
            "Tab"});
            this.comboSymbols.Location = new System.Drawing.Point(123, 35);
            this.comboSymbols.Name = "comboSymbols";
            this.comboSymbols.Size = new System.Drawing.Size(175, 21);
            this.comboSymbols.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "csv symbol:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(212, 184);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(192, 48);
            this.btnGenerate.TabIndex = 13;
            this.btnGenerate.Text = "Generate database";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnenumerateFiles);
            this.groupBox1.Controls.Add(this.btnenumerateDirectories);
            this.groupBox1.Controls.Add(this.btnenumerateAll);
            this.groupBox1.Location = new System.Drawing.Point(16, 258);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(831, 86);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // btnenumerateFiles
            // 
            this.btnenumerateFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenumerateFiles.Location = new System.Drawing.Point(6, 25);
            this.btnenumerateFiles.Name = "btnenumerateFiles";
            this.btnenumerateFiles.Size = new System.Drawing.Size(272, 48);
            this.btnenumerateFiles.TabIndex = 5;
            this.btnenumerateFiles.Text = "Enumerate source files";
            this.btnenumerateFiles.UseVisualStyleBackColor = true;
            this.btnenumerateFiles.Click += new System.EventHandler(this.btnenumerateFiles_Click);
            // 
            // btnenumerateDirectories
            // 
            this.btnenumerateDirectories.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenumerateDirectories.Location = new System.Drawing.Point(284, 25);
            this.btnenumerateDirectories.Name = "btnenumerateDirectories";
            this.btnenumerateDirectories.Size = new System.Drawing.Size(272, 48);
            this.btnenumerateDirectories.TabIndex = 6;
            this.btnenumerateDirectories.Text = "Enumerate source directory";
            this.btnenumerateDirectories.UseVisualStyleBackColor = true;
            this.btnenumerateDirectories.Click += new System.EventHandler(this.btnenumerateDirectories_Click);
            // 
            // btnenumerateAll
            // 
            this.btnenumerateAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnenumerateAll.Location = new System.Drawing.Point(573, 25);
            this.btnenumerateAll.Name = "btnenumerateAll";
            this.btnenumerateAll.Size = new System.Drawing.Size(252, 48);
            this.btnenumerateAll.TabIndex = 7;
            this.btnenumerateAll.Text = "Enumerate all";
            this.btnenumerateAll.UseVisualStyleBackColor = true;
            this.btnenumerateAll.Click += new System.EventHandler(this.btnenumerateAll_Click);
            // 
            // btnEditfolder
            // 
            this.btnEditfolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditfolder.Enabled = false;
            this.btnEditfolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditfolder.Location = new System.Drawing.Point(10, 560);
            this.btnEditfolder.Name = "btnEditfolder";
            this.btnEditfolder.Size = new System.Drawing.Size(162, 48);
            this.btnEditfolder.TabIndex = 11;
            this.btnEditfolder.Text = "Edit folder";
            this.btnEditfolder.UseVisualStyleBackColor = true;
            this.btnEditfolder.Click += new System.EventHandler(this.btnEditfolder_Click);
            // 
            // txtResultGrid
            // 
            this.txtResultGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResultGrid.Location = new System.Drawing.Point(16, 351);
            this.txtResultGrid.Multiline = true;
            this.txtResultGrid.Name = "txtResultGrid";
            this.txtResultGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultGrid.Size = new System.Drawing.Size(830, 204);
            this.txtResultGrid.TabIndex = 10;
            this.txtResultGrid.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtDestPath
            // 
            this.txtDestPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestPath.Location = new System.Drawing.Point(212, 115);
            this.txtDestPath.Name = "txtDestPath";
            this.txtDestPath.Size = new System.Drawing.Size(446, 26);
            this.txtDestPath.TabIndex = 9;
            // 
            // txtSourcePath
            // 
            this.txtSourcePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSourcePath.Location = new System.Drawing.Point(212, 58);
            this.txtSourcePath.Name = "txtSourcePath";
            this.txtSourcePath.Size = new System.Drawing.Size(446, 26);
            this.txtSourcePath.TabIndex = 8;
            this.txtSourcePath.Text = "C:\\Source-Images genesys June2019";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Destination Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Source Path:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(925, 39);
            this.panel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(10, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Manage your picture database";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 651);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Form1";
            this.Text = "Picture Manager - Main";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDestPath;
        private System.Windows.Forms.TextBox txtSourcePath;
        private System.Windows.Forms.Button btnenumerateAll;
        private System.Windows.Forms.Button btnenumerateDirectories;
        private System.Windows.Forms.Button btnenumerateFiles;
        private System.Windows.Forms.TextBox txtResultGrid;
        private System.Windows.Forms.Button btnEditfolder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboSymbols;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelEnumeratedfiles;
    }
}

