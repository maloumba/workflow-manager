﻿namespace rbcEntities
{
    /// <summary>
    /// Describes the type of a field used as identifier
    /// </summary>
    public class IdentifierType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}