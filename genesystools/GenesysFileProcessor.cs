﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace genesystools
{
    class GenesysFileProcessor
    {
       
        static  void Main()
        {
            Console.WriteLine("****Genesys Pictures files formatter***");
                    Console.Write("enter the picture source Path:");
            string txtSourcePath = "C:\\PictureDatabase\\ACCESSIONS";// Console.ReadLine();
                   Console.Write("enter destination Path:");
            string txtDestinationPath = "C:\\GGaccessionsPictures";// Console.ReadLine();
            string answ = "";
            Console.Write("Do you want to generate a database based on source directory?:");
            answ = Console.ReadLine();

            if (answ.Equals("y", StringComparison.CurrentCultureIgnoreCase))
                CreateDatabase("database", txtSourcePath);

            Console.Write("enumerateall files and directories in the source Path?:");
            answ = Console.ReadLine();
            if(answ.Equals("y",StringComparison.CurrentCultureIgnoreCase))
                foreach (var item in Directory.EnumerateFileSystemEntries(txtSourcePath))
                {
                    Console.WriteLine(item);
                } 
 datasourcedialog: Console.Write("Please, enter csv database Path:");
                   string DatabasePath = Console.ReadLine();
                   Console.Write("Please, enter csv separator charactere:");
              ConsoleKeyInfo infotouch= Console.ReadKey(false);
            char csvSeparator = infotouch.KeyChar;
            Repository repository = new Repository();
            try
            {
                repository = new Repository(DatabasePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(3000);
                Console.Clear();
                goto datasourcedialog;
            }
           
            int fileCount = 0;
            //Move files from the "root source directory" to a "subRepertory" with Accenumb as "Repertory name"
            //in the Dest repertory main repertory.
            foreach (var item in Directory.EnumerateFiles(txtSourcePath))
            {
                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 5);
                Console.WriteLine(fileName);

                var accession = repository.GetAcceNumb(fileName, csvSeparator);
                if (accession == null)
                    continue;
               
                if (!Directory.Exists(txtDestinationPath + $"\\{accession}"))
                    Directory.CreateDirectory(txtDestinationPath + $"\\{accession}");
                File.Copy(item, txtDestinationPath + $"\\{accession}\\{fileName}.JPG",true);
                fileCount++;
            }
            //Enumerates directories in the source root folder
            foreach (var item in Directory.EnumerateDirectories(txtSourcePath))
            {
                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 1);
                var accession = repository.GetAcceNumb(fileName, csvSeparator);
                if (accession == null)
                    continue;
                string DestPath = txtDestinationPath + $"\\{accession}";
                if (!Directory.Exists(txtDestinationPath + $"\\{accession}"))
                    Directory.CreateDirectory(DestPath);       
                Directory.Move(item, DestPath);
                fileCount++;
            }
            Console.WriteLine($"{fileCount} files transfered!");
            Console.ReadKey();
        }
        /// <summary>
        /// Create a csv file containing the list of folders names 
        /// or file names in the source directory provided
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="sourcePAth"></param>
        static void CreateDatabase(string databaseName,string sourcePAth)
        {
            using (StreamWriter writer = new StreamWriter($"{databaseName}.csv",false))
            {
                writer.WriteLine("FileName"+"\t"+"FilePath\tAccenumb");
                foreach (var item in Directory.EnumerateFileSystemEntries(sourcePAth).OrderBy(a => a.Any()))
                {
                    writer.WriteLine($"{item}\t{item}") ;
                }
                Console.WriteLine("Database created!");
            }           
        }
    }


    class Repository
    {
        private string _currentfilePath;
        public Repository()
        {

        }
        public Repository(string filePath)
        {
           
            if(File.Exists(filePath))
            {
                Console.WriteLine("This file has been found!");
                _currentfilePath = filePath;
            }
            else
            {
                throw new Exception("The file specified does not exist!");
            }
        }


       internal Dictionary<string, string> GetfileContent(string filepath,char csvsymbole)
        {
            Dictionary<string, string> datafile = new Dictionary<string, string>();//Key/Value pair file data for accenumb and accename

            StreamReader reader = new StreamReader(File.OpenRead(filepath));
            string[] lignes =  reader.ReadToEnd().Split(Environment.NewLine);
            for (int i = 0; i < lignes.Length-1; i++)
            {

                string[] keyvalue = lignes[i].Split(new char[] { csvsymbole });
                datafile[keyvalue[0]] = keyvalue[1];
            }
            return datafile;
        }

       internal string GetAcceNumb(string accename,char csvsymbole)
        {
            var data= GetfileContent(_currentfilePath, csvsymbole);
            string accenumb=  data.FirstOrDefault(a => a.Value.Equals(accename)).Key;
          //Return the accenumb which is the key in the dictionary
            return accenumb;
        }

    }
}
