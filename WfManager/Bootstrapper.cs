﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfManager.Core;

namespace WfManager
{
    public static class Bootstrapper
    {
        public static void Init()
        {
            DependencyInjector.Register<IUnitOfWork, UnitOfWork>();
        }
    }
}
