﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicturesManager
{
    public partial class Form1 : Form
    {
        string _databaseName = null;
        DataTable _dt;
        public Form1()
        {
            InitializeComponent();
            _databaseName = "_dbgenerated.csv";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSourcePath.Text))
            {
                DataTable dt = GetDataTableStructure();
                //MessageBox.Show(dt.Rows.Count.ToString());
                if(comboSymbols.SelectedIndex==-1)
                {
                    MessageBox.Show("You must first select the default application csv format!", "Picture Manager Error");
                    return;
                }
                char csvsymbole = comboSymbols.SelectedItem.ToString().Equals("comma") ? ',' : '\t';
                frmEditdatabase objdatabase = new frmEditdatabase(dt,csvsymbole);
               if( objdatabase.ShowDialog()==DialogResult.OK)
                {
                    this._dt = objdatabase.Database;
                    btnEditfolder.Enabled = true;
                }
            }
        }

        private DataTable GetDataTableStructure()
        {
           // _repository = new Repository(txtSourcePath.Text);
            DataTable dt = new DataTable(_databaseName);
            dt.Columns.Add("ACCENUMB");
            dt.Columns.Add("ACCENAME");
            dt.Columns.Add("PATH");
            dt.Columns.Add("TYPE");
            dt.Columns.Add("DESCRIPTION");

            //enumerate directories in the source repertory
            foreach (var item in Directory.EnumerateDirectories(txtSourcePath.Text))
            {
                string fileName = getFileName(item,true);
                DataRow newrow = dt.NewRow();
                newrow["ACCENAME"] = fileName;
                newrow["PATH"] = item;
                newrow["TYPE"] = "Directory";
                dt.Rows.Add(newrow);
            }
            //enumerate files in the source repertory
            foreach (var item in Directory.EnumerateFiles(txtSourcePath.Text))
            {
                string fileName = getFileName(item,false);
                DataRow newrow = dt.NewRow();
                newrow["ACCENAME"] = fileName;
                newrow["PATH"] = item;
                newrow["TYPE"] = item.Substring(item.LastIndexOf("."), 4);
                dt.Rows.Add(newrow);
            }
            return dt;
        }

        private static string getFileName(string item,bool keepExtension)
        {
            int lastIndexofSeparator = item.LastIndexOf("\\");
            string fileName = null;
            if (keepExtension)
             fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 1);
            else
                fileName= fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator)-5);
            return fileName;
        }




        /// <summary>
        /// Create a csv file containing the list of folders names 
        /// or file names in the source directory provided
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="sourcePAth"></param>
        void CreateDatabase(string databaseName, string sourcePAth)
        {
            using (StreamWriter writer = new StreamWriter($"{databaseName}.csv", false))
            {
                writer.WriteLine("FileName" + "\t" + "FilePath\tAccenumb");
                foreach (var item in Directory.EnumerateFileSystemEntries(sourcePAth).OrderBy(a => a.Any()))
                {
                    writer.WriteLine($"{item}\t{item}");
                }
                MessageBox.Show("Database created!");
            }
        }

        private void btnenumerateAll_Click(object sender, EventArgs e)
        {
            txtResultGrid.Text = string.Empty;
            int fileCount = 0;
            foreach (var item in Directory.EnumerateFileSystemEntries(txtSourcePath.Text))
            {
                txtResultGrid.Text += item + Environment.NewLine;
                fileCount++;
            }
            labelEnumeratedfiles.Text = $"{fileCount} files founded!";
        }

        private void btnenumerateDirectories_Click(object sender, EventArgs e)
        {
            txtResultGrid.Text = string.Empty;
            int fileCount = 0;
            foreach (var item in Directory.EnumerateDirectories(txtSourcePath.Text))
            {
                txtResultGrid.Text += item + Environment.NewLine;
                fileCount++;
            }
            labelEnumeratedfiles.Text = $"{fileCount} files founded!";
        }

        private void btnenumerateFiles_Click(object sender, EventArgs e)
        {
            txtResultGrid.Text = string.Empty;
            int fileCount = 0;
            foreach (var item in Directory.EnumerateFiles(txtSourcePath.Text))
            {
                txtResultGrid.Text += item + Environment.NewLine;
                fileCount++;
            }
            labelEnumeratedfiles.Text = $"{fileCount} files founded!";
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if(dialog.ShowDialog()==DialogResult.OK)
            {
                _databaseName = dialog.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnEditfolder_Click(object sender, EventArgs e)
        {
            if(_dt==null||string.IsNullOrEmpty(txtDestPath.Text)||string.IsNullOrEmpty(txtSourcePath.Text))
            {
                MessageBox.Show($"All the configuration are not set.You have to set first:" +
                    $"1-{Environment.NewLine}the database.{Environment.NewLine}" +
                    $"2-The Source and Destination folders paths.");
            }

          int fileMoved=  MovePictures(_dt.Rows);
            MessageBox.Show($"{fileMoved} files have been moved");
        }

        int MovePictures(DataRowCollection rowcollection)
        {
            //Move files from the "root source directory" to a "subRepertory" with Accenumb as "Repertory name"
            //in the Dest repertory main repertory.
            int fileCount = 0;
    
            foreach (var item in rowcollection)
            {
                DataRow row = item as DataRow;
                string fileName = row["ACCENAME"].ToString();
                Console.WriteLine(fileName);

                var accession = row["ACCENUMB"].ToString();
                if (string.IsNullOrEmpty(accession))
                    continue;

                if (!Directory.Exists(txtDestPath.Text + $"\\{accession}"))
                    Directory.CreateDirectory(txtDestPath.Text + $"\\{accession}");

                string destpath = txtDestPath.Text + $"\\{accession}\\{fileName}.JPG";
                string sourcepath = row["PATH"].ToString();
                try
                {
                    //copy the entire directory if sourcepath representing a directory
                    if (row["TYPE"].ToString().Equals("Directory"))
                    {
                        foreach (string file in Directory.EnumerateFiles(sourcepath))
                        {

                            string subfileName = getFileName(file,false);
                            string finalpath = txtDestPath.Text + $"\\{accession}\\{subfileName}";
                            File.Copy(file, finalpath, true);
                        }

                    }
                    else
                    {
                        //copy single files
                        File.Copy(sourcepath, destpath, true);
                        fileCount++;
                    }
                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
               
            }
            return fileCount;
           
        }
    }
}
