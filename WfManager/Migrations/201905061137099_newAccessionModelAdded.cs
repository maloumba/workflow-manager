namespace WfManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newAccessionModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.gg_inventory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Prefix = c.String(),
                        Accenumb = c.String(),
                        Position = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pas_Accession",
                c => new
                    {
                        AccessionId = c.String(nullable: false, maxLength: 128),
                        Accename = c.String(),
                    })
                .PrimaryKey(t => t.AccessionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pas_Accession");
            DropTable("dbo.gg_inventory");
        }
    }
}
