﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataAccess
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly RbcDbContext _dbContext;
        public GenericRepository(RbcDbContext objContext)
        {
            _dbContext = objContext;
        }
        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = _dbContext.Set<T>();
            //Apply eager loading       
            if (navigationProperties != null)
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);
            list = dbQuery.AsNoTracking().ToList();
            return list;
        }



        public virtual IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;

            IQueryable<T> dbQuery = _dbContext.Set<T>();
            //Apply eager loading          
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include(navigationProperty);

            list = where != null ? dbQuery.AsNoTracking()
                .Where(where)
                .ToList() : dbQuery.ToList();

            return list;
        }

        public virtual T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            T item;

            IQueryable<T> dbQuery = _dbContext.Set<T>();
            //Apply eager loading  
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include(navigationProperty);
            item = dbQuery
                .AsNoTracking() //Don't track any changes for the selected item     
                .FirstOrDefault(where); //Apply where clause  
            return item;
        }


        public void Add(params T[] items)
        {
            foreach (T item in items)
            {
                _dbContext.Entry(item).State = EntityState.Added;
            }
        }

        public void Update(params T[] items)
        {
            foreach (T item in items)
            {                  
                 _dbContext.Entry(item).State= EntityState.Modified; // This should attach entity
            }       
        }
        public void Remove(params T[] items)
        {
            foreach (T item in items)
            {
                 _dbContext.Entry(item).State = EntityState.Deleted; // This should attach entity

            }
        }
    }
}
