﻿namespace PicturesManager
{
    partial class frmEditdatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.PictureGrid = new System.Windows.Forms.DataGridView();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAccenumb = new System.Windows.Forms.TextBox();
            this.btnBatchUpdate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnshowPicture = new System.Windows.Forms.Button();
            this.labelDirectories = new System.Windows.Forms.Label();
            this.labelfiles = new System.Windows.Forms.Label();
            this.btnMatch = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.linkReset = new System.Windows.Forms.LinkLabel();
            this.labelsearchresult = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxSymbol = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDefaultDescription = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1148, 39);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(9, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edit picture database";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 22);
            this.label2.TabIndex = 4;
            this.label2.Text = "Search:";
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(99, 100);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(360, 30);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // PictureGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.PictureGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.PictureGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.PictureGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PictureGrid.Location = new System.Drawing.Point(16, 151);
            this.PictureGrid.Name = "PictureGrid";
            this.PictureGrid.RowTemplate.Height = 28;
            this.PictureGrid.Size = new System.Drawing.Size(843, 334);
            this.PictureGrid.TabIndex = 6;
            this.PictureGrid.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PictureGrid_RowHeaderMouseClick);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(16, 492);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(146, 48);
            this.btnGenerate.TabIndex = 14;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(640, 505);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 25);
            this.label3.TabIndex = 15;
            this.label3.Text = "Accenumb:";
            // 
            // txtAccenumb
            // 
            this.txtAccenumb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccenumb.Enabled = false;
            this.txtAccenumb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccenumb.Location = new System.Drawing.Point(758, 502);
            this.txtAccenumb.Name = "txtAccenumb";
            this.txtAccenumb.Size = new System.Drawing.Size(229, 30);
            this.txtAccenumb.TabIndex = 16;
            this.txtAccenumb.TextChanged += new System.EventHandler(this.txtAccenumb_TextChanged);
            // 
            // btnBatchUpdate
            // 
            this.btnBatchUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatchUpdate.Enabled = false;
            this.btnBatchUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBatchUpdate.Location = new System.Drawing.Point(994, 492);
            this.btnBatchUpdate.Name = "btnBatchUpdate";
            this.btnBatchUpdate.Size = new System.Drawing.Size(146, 48);
            this.btnBatchUpdate.TabIndex = 17;
            this.btnBatchUpdate.Text = "Update";
            this.toolTip1.SetToolTip(this.btnBatchUpdate, "Batch update accenumb");
            this.btnBatchUpdate.UseVisualStyleBackColor = true;
            this.btnBatchUpdate.Click += new System.EventHandler(this.btnBatchUpdate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnshowPicture);
            this.groupBox1.Controls.Add(this.labelDirectories);
            this.groupBox1.Controls.Add(this.labelfiles);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(867, 120);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(281, 364);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // btnshowPicture
            // 
            this.btnshowPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnshowPicture.Enabled = false;
            this.btnshowPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnshowPicture.Location = new System.Drawing.Point(59, 156);
            this.btnshowPicture.Name = "btnshowPicture";
            this.btnshowPicture.Size = new System.Drawing.Size(146, 48);
            this.btnshowPicture.TabIndex = 18;
            this.btnshowPicture.Text = "Show Pict";
            this.btnshowPicture.UseVisualStyleBackColor = true;
            this.btnshowPicture.Click += new System.EventHandler(this.btnshowPicture_Click);
            // 
            // labelDirectories
            // 
            this.labelDirectories.AutoSize = true;
            this.labelDirectories.Location = new System.Drawing.Point(10, 83);
            this.labelDirectories.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDirectories.Name = "labelDirectories";
            this.labelDirectories.Size = new System.Drawing.Size(20, 22);
            this.labelDirectories.TabIndex = 1;
            this.labelDirectories.Text = "//";
            // 
            // labelfiles
            // 
            this.labelfiles.AutoSize = true;
            this.labelfiles.Location = new System.Drawing.Point(10, 31);
            this.labelfiles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelfiles.Name = "labelfiles";
            this.labelfiles.Size = new System.Drawing.Size(20, 22);
            this.labelfiles.TabIndex = 0;
            this.labelfiles.Text = "//";
            // 
            // btnMatch
            // 
            this.btnMatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMatch.Location = new System.Drawing.Point(168, 492);
            this.btnMatch.Name = "btnMatch";
            this.btnMatch.Size = new System.Drawing.Size(146, 48);
            this.btnMatch.TabIndex = 19;
            this.btnMatch.Text = "Match";
            this.toolTip1.SetToolTip(this.btnMatch, "Match Accename with another database file to automatically update your database A" +
        "CCENUMB");
            this.btnMatch.UseVisualStyleBackColor = true;
            this.btnMatch.Click += new System.EventHandler(this.btnMatch_Click);
            // 
            // linkReset
            // 
            this.linkReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkReset.AutoSize = true;
            this.linkReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkReset.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkReset.Location = new System.Drawing.Point(780, 120);
            this.linkReset.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkReset.Name = "linkReset";
            this.linkReset.Size = new System.Drawing.Size(79, 25);
            this.linkReset.TabIndex = 20;
            this.linkReset.TabStop = true;
            this.linkReset.Text = "Reload";
            this.linkReset.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkReset_LinkClicked);
            // 
            // labelsearchresult
            // 
            this.labelsearchresult.AutoSize = true;
            this.labelsearchresult.Location = new System.Drawing.Point(466, 109);
            this.labelsearchresult.Name = "labelsearchresult";
            this.labelsearchresult.Size = new System.Drawing.Size(17, 20);
            this.labelsearchresult.TabIndex = 21;
            this.labelsearchresult.Text = "//";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(270, 22);
            this.label4.TabIndex = 23;
            this.label4.Text = "Name and description separator:";
            // 
            // comboBoxSymbol
            // 
            this.comboBoxSymbol.FormattingEnabled = true;
            this.comboBoxSymbol.Items.AddRange(new object[] {
            "#",
            "-",
            "_",
            "&"});
            this.comboBoxSymbol.Location = new System.Drawing.Point(313, 50);
            this.comboBoxSymbol.Name = "comboBoxSymbol";
            this.comboBoxSymbol.Size = new System.Drawing.Size(110, 28);
            this.comboBoxSymbol.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(488, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 22);
            this.label5.TabIndex = 25;
            this.label5.Text = "Search:";
            // 
            // txtDefaultDescription
            // 
            this.txtDefaultDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDefaultDescription.Location = new System.Drawing.Point(579, 50);
            this.txtDefaultDescription.Name = "txtDefaultDescription";
            this.txtDefaultDescription.Size = new System.Drawing.Size(258, 30);
            this.txtDefaultDescription.TabIndex = 26;
            // 
            // frmEditdatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1148, 548);
            this.Controls.Add(this.txtDefaultDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxSymbol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelsearchresult);
            this.Controls.Add(this.linkReset);
            this.Controls.Add(this.btnMatch);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnBatchUpdate);
            this.Controls.Add(this.txtAccenumb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.PictureGrid);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Name = "frmEditdatabase";
            this.Text = "Picture Manager - Database";
            this.Load += new System.EventHandler(this.frmEditdatabase_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView PictureGrid;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAccenumb;
        private System.Windows.Forms.Button btnBatchUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelDirectories;
        private System.Windows.Forms.Label labelfiles;
        private System.Windows.Forms.Button btnMatch;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnshowPicture;
        private System.Windows.Forms.LinkLabel linkReset;
        private System.Windows.Forms.Label labelsearchresult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxSymbol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDefaultDescription;
    }
}