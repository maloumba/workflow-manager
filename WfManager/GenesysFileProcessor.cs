﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WfManager
{
    class GenesysFileProcessor
    {
       
        static void Main()
        {
            Console.WriteLine("****Genesys Pictures files formatter***");
                    Console.Write("enter the picture source Path:");
                    string txtSourcePath = Console.ReadLine();
                   Console.Write("enter destination Path:");
                   string txtDestinationPath = Console.ReadLine();
 datasourcedialog: Console.Write("Please, enter csv database Path:");
                   string DatabasePath = Console.ReadLine();
                   Console.Write("Please, enter csv separator charactere:");
              ConsoleKeyInfo infotouch= Console.ReadKey(false);
            char csvSeparator = infotouch.KeyChar;
            Repository repository = new Repository("");
            try
            {
                repository = new Repository(DatabasePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Thread.Sleep(3000);
                Console.Clear();
                goto datasourcedialog;
            }
            
            int fileCount = 0;
            //Move files from the "root source directory" to a "subRepertory" with Accenumb as "Repertory name"
            //in the Dest repertory main repertory.
            foreach (var item in Directory.EnumerateFiles(txtSourcePath))
            {
                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 5);
                Console.WriteLine(fileName);
                var accession = repository.GetAcceNumb(fileName,csvSeparator);
                if (accession == null)
                    continue;
                if (!Directory.Exists(txtDestinationPath + $"\\{accession}"))
                    Directory.CreateDirectory(txtDestinationPath + $"\\{accession}");
                File.Copy(item, txtDestinationPath + $"\\{accession}\\{fileName}.JPG");
                fileCount++;
            }
            //Enumerates directories in the source root folder
            foreach (var item in Directory.EnumerateDirectories(txtSourcePath))
            {

                int lastIndexofSeparator = item.LastIndexOf("\\");
                string fileName = item.Substring(lastIndexofSeparator + 1, (item.Length - lastIndexofSeparator) - 1);
                var accession = repository.GetAcceNumb(fileName,csvSeparator);
                if (accession == null)
                    continue;
                if (!Directory.Exists(txtDestinationPath + $"\\{accession}"))
                    Directory.Move(item, txtDestinationPath + $"\\{accession}");
                fileCount++;
                Console.WriteLine(item);
            }
            Console.WriteLine($"{fileCount} files transfered");
        }
    }


    class Repository
    {
        private string _currentfilePath;
        public Repository(string filePath)
        {
           
            if(File.Exists(filePath))
            {
                Console.WriteLine("This file has been found!");
                _currentfilePath = filePath;
            }
            else
            {
                throw new Exception("The file specified does not exist!");
            }
        }


       internal async Task<Dictionary<string, string>> getfileContentAsync(string filepath,char csvsymbole)
        {
            Dictionary<string, string> datafile = new Dictionary<string, string>();//Key/Value pair file data for accenumb and accename
            StreamReader reader = new StreamReader(File.OpenRead(filepath));
            string lignes = await reader.ReadToEndAsync();
            for (int i = 0; i < lignes.Length; i++)
            {
                string[] oneLigntable = lignes.Split(new char[] { csvsymbole });

                datafile[oneLigntable[0]] = oneLigntable[1];
            }
            return datafile;
        }

       internal async Task<string> GetAcceNumb(string accename,char csvsymbole)
        {
            var data=await getfileContentAsync(_currentfilePath, csvsymbole);
            int i = 0;
            while (data.Keys==null)
            {
                Console.Write("|");
                Thread.Sleep(500);
                // Thread.SpinWait
                Console.Write("___");
                Thread.Sleep(500);
                Console.Write("|");
                Thread.Sleep(500);
                Console.Write("___");
                Thread.Sleep(500);
                i++;
                if(i==500)
                {
                    Console.SetCursorPosition(5, 300);
                    Console.Write("c=Cancel, other=Continue:");
                    string choice=Console.ReadLine();
                    if (choice.Equals("c", StringComparison.CurrentCultureIgnoreCase))
                        return null;
                }
            }
            string accenumb = data.First(a => a.Value.Equals(accename)).Key;//Return the accenumb which is the key in the dictionary
            return accenumb;
        }

    }
}
