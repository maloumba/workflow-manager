﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WfManager.Entites
{
   public class WorkflowDetail
    {
        /// <summary>
        /// Workflow Detail automatic Identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Workflow Identifier
        /// </summary>
        public int WorkflowId { get; set; }
        public Workflow Workflow { get; set; }
        /// <summary>
        /// Material Identifier
        /// </summary>
        public string MaId { get; set; }
        /// <summary>
        /// Ex:Sample Lot Id,TemporaryID,AccessionId,etc
        /// </summary>
        public IdentifierType IdentifierType { get; set; }
        public int IdentifierTypeId { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Quantity of seeds withdrawn from the MTS
        /// </summary>
        public float? QtyWdrMTS { get; set; }
        /// <summary>
        /// Current quantity of seeds in the coldroom
        /// </summary>
        public float? CurrentQty { get; set; }
        /// <summary>
        /// start date of heating
        /// </summary>
        public DateTime? HeatingstartDate { get; set; }
        /// <summary>
        /// End of Heating date
        /// </summary>
        public DateTime? HeatingEndDate { get; set; }
        /// <summary>
        /// Date of sending the sample to Pre-germination
        /// </summary>
        public DateTime? Pre_germinationDate { get; set; }
        /// <summary>
        /// Date of sending the sample to Pre-germination
        /// </summary>
        public DateTime? NurseryDate { get; set; }
        /// <summary>
        /// Date of sending the sample to the field
        /// </summary>
        public DateTime? SentToFieldDate { get; set; }
        /// <summary>
        /// sowing Date
        /// </summary>
        public DateTime? SowingDate { get; set; }
        /// <summary>
        /// The name or description of the pseron who sowed the sample
        /// </summary>
        public string SowedBy { get; set; }
        /// <summary>
        /// Date of the sample harvest
        /// </summary>
        public DateTime? HarvestDate { get; set; }
        /// <summary>
        /// the name or description of the person who harvested the sample
        /// </summary>
        public string HarvestBy { get; set; }
        /// <summary>
        /// Winnowing date
        /// </summary>
        public DateTime? WinnowingDate { get; set; }
        /// <summary>
        /// Date of the predrying
        /// </summary>
        public DateTime? PreDryDate { get; set; }
        /// <summary>
        /// the start date of the sample drying 
        /// </summary>
        public DateTime? StartDryingDate { get; set; }
        /// <summary>
        /// the date of ending the sample drying
        /// </summary>
        public DateTime? EndDryingDate { get; set; }
        public bool WADried { get; set; }
        /// <summary>
        /// The date where the sample has been packed
        /// </summary>
        public DateTime? PackedForCleaningDate { get; set; }
        /// <summary>
        /// Cleaning start Date
        /// </summary>
        public DateTime? CleaningDate { get; set; }
       
        /// <summary>
        /// Name or description of the pseron who cleaned the sample
        /// </summary>
        public string CleanedBy { get; set; }
        /// <summary>
        /// Value indicating if the sample has been packed for viability Test
        /// </summary>
        public DateTime? PackedForViabTestDate { get; set; }
        /// <summary>
        /// Viability Test Result
        /// </summary>
        public int? ViabTestResult { get; set; }
        /// <summary>
        /// Date of the viability test
        /// </summary>
        public DateTime? ViabTestResultDate { get; set; }
        /// <summary>
        /// Quantity withdrawn to be sent to LTS
        /// </summary>
        public float? QtyWdrLTS { get; set; }
        /// <summary>
        /// Quantity withdrawn to be sent to FC
        /// </summary>
        public float? QtyWdrFC { get; set; }
        /// <summary>
        /// Quantity withdrawn to be sent to SVB
        /// </summary>
        public float? QtyWdrSVB { get; set; }
        /// <summary>
        /// Quantity withdrawn to be sent for Distribution
        /// </summary>
        public float? QtyWdrDist { get; set; }
        /// <summary>
        /// Distribution Identifier
        /// </summary>
        public int DistId { get; set; }
        /// <summary>
        /// Value indicating if the sample has been already sent to the requester
        /// </summary>
        public bool? SampleSentForDist { get; set; }
        /// <summary>
        /// New Identifier for a sample to be introduced in coldRooms
        /// </summary>
        public string NewId { get; set; }
        /// <summary>
        /// Type of the new identifier
        /// </summary>
        public string NewIdentifierType { get; set; }
        /// <summary>
        /// the location of a new sample in the drying room
        /// </summary>
        public string LocationInDryRoom { get; set; }
        /// <summary>
        /// Boolean value indicating if yes or not the sample
        /// has been packed to be introduced in the MTS
        /// </summary>
        public bool? PackedForMTS { get; set; }

        public DateTime? Datestorage { get; set; }

        /// <summary>
        /// Date from which materials have been sent to the seed health unit for treatment
        /// </summary>
        public DateTime? DateSentToHealthUnit { get; set; }

        /// <summary>
        /// Date from which materials came back from health unit after treatment
        /// </summary>
        public DateTime? DateGetFromHealthUnit { get; set; }

        /// <summary>
        /// In Grin-Global?
        /// </summary>
        public bool? InGG { get; set; }
    }
}
