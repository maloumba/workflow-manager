﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WfManager.Entites;

namespace WfManager
{
    public partial class AddWorkflow : Form
    {
        Initiator initiator = DependencyInjector.Retrieve<Initiator>();
        public AddWorkflow()
        {
            InitializeComponent();
        }

        private void AddWorkflow_Load(object sender, EventArgs e)
        {
          comboBoxType.DataSource=  initiator._businessClass.WorkflowTypeRepository.GetAll();
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            Workflow newworkflow = new Workflow
            {
                Name = txtName.Text,
                WorkflowTypeId = (comboBoxType.SelectedItem as WorkflowType).Id,
                Year = int.Parse(comboBoxYear.SelectedItem.ToString()),
                workflowStateId = initiator._businessClass.WorkflowStateRepository.GetSingle(id => id.Name.Equals("Initiated")).Id
            };
            try
            {
                initiator._businessClass.WorkflowRepository.Add(newworkflow);
                initiator._businessClass.Complete();
                MessageBox.Show("A new workflow has been added!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            
        }
    }
}
