﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WfManager.DataAccess;
using WfManager.Entites;

namespace WfManager.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _dbcontext;
        public UnitOfWork(DataContext objContext)
        {
            _dbcontext = objContext;
            IdentifierTypeRepository = new GenericRepository<IdentifierType>(objContext);
            WorkflowRepository = new GenericRepository<Workflow>(objContext);
            ActionbyWorkflowTypeRepository = new GenericRepository<ActionbyWorkflowType>(objContext);
            WorkflowActionRepository = new GenericRepository<WorkflowAction>(objContext);
            WorkflowDetailRepository = new GenericRepository<WorkflowDetail>(objContext);
            WorkflowStateRepository = new GenericRepository<WorkflowState>(objContext);
            WorkflowTypeRepository = new GenericRepository<WorkflowType>(objContext);
            AccessionRepository = new GenericRepository<Pas_Accession>(objContext);
            InventoryRepository = new GenericRepository<gg_inventory>(objContext);
        }

        public IGenericRepository<Workflow> WorkflowRepository { get; private set; }

        public IGenericRepository<ActionbyWorkflowType> ActionbyWorkflowTypeRepository { get; private set; }

        public IGenericRepository<IdentifierType> IdentifierTypeRepository { get; private set; }

        public IGenericRepository<WorkflowAction> WorkflowActionRepository { get; private set; }

        public IGenericRepository<WorkflowDetail> WorkflowDetailRepository { get; private set; }

        public IGenericRepository<WorkflowState> WorkflowStateRepository { get; private set; }

        public IGenericRepository<WorkflowType> WorkflowTypeRepository { get; private set; }

        public IGenericRepository<gg_inventory> InventoryRepository { get; private set; }

        public IGenericRepository<Pas_Accession> AccessionRepository { get; private set; }

        public void Complete()
        {
            _dbcontext.SaveChanges();
        }
    }
}
