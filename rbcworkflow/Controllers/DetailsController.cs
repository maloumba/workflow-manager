﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using rbcEntities;
using rbcworkflow.ServiceModels;
using rbcworkflowCore;

namespace rbcworkflow.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetailsController : ControllerBase
    {
        IUnitOfWork _unitofwork;
        readonly IMapper _maper;
        public DetailsController(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitofwork = unitOfWork;
            _maper = mapper;
        }
      
       
        [HttpPost]
        public ActionResult Post([FromBody]WorkflowDetail value)
        {
            if(ModelState.IsValid)
            _unitofwork.WorkflowDetailRepository.Add(value);
            _unitofwork.Complete();
            return CreatedAtAction(nameof(Get), new { id = value.Id }, value);
        }

        [HttpGet("workflowdetail/{id}")]
        public ActionResult<WorkflowDetail> Get(int id)
        {
            var workflowdetail = _unitofwork.WorkflowDetailRepository.GetSingle(i => i.Id == id);
            if (!(workflowdetail is WorkflowDetail))
            {
                return NotFound();
            }
            return workflowdetail;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, WorkflowDetail value)
        {
            if (id != value.Id) { return BadRequest(); }
            _unitofwork.WorkflowDetailRepository.Update(value);
            _unitofwork.Complete();
            return NoContent();
        }

        // PUT api/<controller>/5
        [HttpPatch("{id}")]
        public ActionResult Patch(int id,[FromBody]JsonPatchDocument<DetailClient> detailPatch)
        {
            
            var detail = _unitofwork.WorkflowDetailRepository.GetSingle(i=>i.Id==id);
            if (!(detail is WorkflowDetail)) return NotFound();
            
            DetailClient detailclient = _maper.Map<DetailClient>(detail);
            detailPatch.ApplyTo(detailclient);
           
                     
            try
            {
                _maper.Map(detailclient, detail);
                _unitofwork.WorkflowDetailRepository.Update(detail);
                _unitofwork.Complete();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }       
            return Ok(_maper.Map<DetailClient>(detail));
        }
    }
}